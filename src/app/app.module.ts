import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CounterDisplayComponent } from './counter-display/counter-display.component';
import { ButtonsComponent } from './buttons/buttons.component';
import { HistoryDisplayComponent } from './history-display/history-display.component';

@NgModule({
  declarations: [
    AppComponent,
    CounterDisplayComponent,
    ButtonsComponent,
    HistoryDisplayComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
